import express from "express";
import { AppDataSource } from "./data-source";
import gamesRouter from "./routes/games";
const morgan = require('morgan')

const app = express();

app.use(morgan('tiny'))
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.get("/", (req, res) => {
  res.send("Hello World")
})

app.use('/games', gamesRouter)

AppDataSource.initialize().then(() => {
  const PORT = process.env.PORT || 3000;

  app.listen(PORT, () => {
    console.log(`Server is running in http://localhost:${PORT}`)
  })

})
.catch(error => console.log(error));