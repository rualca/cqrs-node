import "reflect-metadata"
import { DataSource } from "typeorm";

export const AppDataSource = new DataSource({
  "type": "sqlite",
  "database": "./data/games.db",
  "synchronize": true,
  "logging": true,
  "entities": [
    "src/entities/**/*.ts"
  ],
  "migrations": [
    "src/migration/**/*.ts"
  ],
  "subscribers": [
    "src/subscriber/**/*.ts"
  ]
})