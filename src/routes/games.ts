import express from 'express'
import { GameCommandFactory } from '../commands/application/games/GameCommandFactory';

const gamesRouter = express.Router()

import { Games } from "../entities/Games";

const gameCommandFactory = new GameCommandFactory()

gamesRouter.post('/create', (req, res, next) => {
  console.log(req.body);
  
  const game: Games = req.body
  const commandName = 'POSTCommand'

  const commandConfig = {
    commandName,
    args: game
  }

  const command = gameCommandFactory.makeCommand(commandConfig)
  const results = command.execute()
  const statusCode = results.status ? 200 : 500

  res.status(statusCode).send('foo')
})

export default gamesRouter