import { POSTCommand } from './POSTGameCommand'
import CommandConfig from '../../../common/CommandConfig'

export class GameCommandFactory<CommandFactory> {
    makeCommand = (config: CommandConfig) => {
      if (config.commandName !== POSTCommand.name) {
        throw new Error('Command not found!')
      }
      
      return new POSTCommand(config.args)
    }
}