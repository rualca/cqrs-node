import { Games } from "../../../entities/Games";
import { AppDataSource } from "../../../data-source";


export class CreateGame<Command> {
  private game: Games;

  constructor(game: Games) {
    this.game = game
  }

  public execute = () => {
    const game = new Games()

    Object.keys(this.game).forEach(
      (key) => game[key] = this.game[key]
    )

    const status = AppDataSource.getRepository(Games)
      .save(game)
      .then(game => {
        console.log(`game added success: ${game.id}`)
        
        return true
      })
      .catch(err => {
        console.log(`error adding game: ${err}`)
        
        return
      })

    return { status }
  }
}
