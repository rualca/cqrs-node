import CommandConfig from '../../../common/CommandConfig'
import { CreateGame } from './CreateGame'


export class CRUDCommandFactory<CommandFactory> {
  makeCommand(config: CommandConfig) {
    if (config.commandName !== CreateGame.name) {
      throw new Error('Command not found!')
    }
    
    return new CreateGame(config.args)
  }
}