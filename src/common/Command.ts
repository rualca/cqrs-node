import { CommandResponse } from './CommandResponse'

export default interface Command {
    execute: () => CommandResponse
}