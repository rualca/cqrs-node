import Command from './Command'
import CommandConfig from './CommandConfig'

export default interface CommandFactory {
    makeCommand: (config: CommandConfig) => Command
}